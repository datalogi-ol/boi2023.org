# BOI 2023 website

To be published at [boi2023.org](https://boi2023.org). Maintainer: Christian Bertram / `cbertram`.

The page is built using [SvelteKit](https://kit.svelte.dev) and [Supabase](https://supabase.com).

Currently hosted on [Cloudflare](https://cloudflare.com).

## 🧞 Commands

All commands are run from the root of the project:

| Command              | Action                      |
| :------------------- | :-------------------------- |
| `npm ci`             | Install dependencies        |
| `npm run dev`        | Start local dev server      |
| `npx supabase start` | Start local Supabase server |
| `npm run format`     | Format all code             |
| `npm run lint`       | Run linter                  |
