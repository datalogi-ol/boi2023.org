alter table "public"."profile" add column avatar_url text unique;

drop function public.get_export_profile(text);
drop view "public"."export_profile";
create view "public"."export_profile" with (security_invoker) as SELECT seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.birthdate,
    profile.mobile_phone,
    profile.passport_nationality,
    profile.eduroam_access,
    profile.diet_requirements,
    profile.special_needs,
    profile.consent,
    profile.avatar_url,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
   FROM ((((profile
     JOIN seq_id ON ((profile.id = seq_id.id)))
     JOIN profile_type ON ((profile.id = profile_type.id)))
     JOIN auth.users ON ((profile.id = users.id)))
     LEFT JOIN country ON ((profile_type.country_alpha3 = country.alpha3)));

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.get_export_profile(sk text)
 RETURNS SETOF export_profile
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
  select seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.birthdate,
    profile.mobile_phone,
    profile.passport_nationality,
    profile.eduroam_access,
    profile.diet_requirements,
    profile.special_needs,
    profile.consent,
    profile.avatar_url,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
    from profile
    join seq_id on profile.id=seq_id.id
    join profile_type on profile.id=profile_type.id
    join auth.users on profile.id=auth.users.id
    left join country on profile_type.country_alpha3=country.alpha3
    where exists (select * from server_key where server_key.server_key=sk);
$function$
;

insert into storage.buckets (id, name)
values ('avatar', 'avatar');

create extension if not exists "http" with schema "extensions";

create or replace function delete_storage_object(bucket text, object text, out status int, out content text)
returns record
language 'plpgsql'
security definer
as $$
declare
  project_url text := 'http://localhost:54321';
  service_role_key text := 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImV4cCI6MTk4MzgxMjk5Nn0.EGIM96RAZx35lJzdJsyH-qQwv8Hdp7fsn3W0YpN81IU'; --  full access needed
  url text := project_url||'/storage/v1/object/'||bucket||'/'||object;
begin
  select
      into status, content
           result.status::int, result.content::text
      FROM extensions.http((
    'DELETE',
    url,
    ARRAY[extensions.http_header('authorization','Bearer '||service_role_key)],
    NULL,
    NULL)::extensions.http_request) as result;
end;
$$;

create or replace function delete_avatar(avatar_url text, out status int, out content text)
returns record
language 'plpgsql'
security definer
as $$
begin
  select
      into status, content
           result.status, result.content
      from public.delete_storage_object('avatar', avatar_url) as result;
end;
$$;

create or replace function delete_old_avatar()
returns trigger
language 'plpgsql'
security definer
as $$
declare
  status int;
  content text;
begin
  if coalesce(old.avatar_url, '') <> ''
      and (tg_op = 'DELETE' or (old.avatar_url <> new.avatar_url)) then
    select
      into status, content
      result.status, result.content
      from public.delete_avatar(old.avatar_url) as result;
    if status <> 200 then
      raise warning 'Could not delete avatar: % %', status, content;
    end if;
  end if;
  if tg_op = 'DELETE' then
    return old;
  end if;
  return new;
end;
$$;

create trigger before_profile_changes
  before update of avatar_url or delete on public.profile
  for each row execute function public.delete_old_avatar();

create policy "Give authenticated users access 1bs1gex_0"
on "storage"."objects"
as permissive
for select
to public
using (((bucket_id = 'avatar'::text) AND (auth.role() = 'authenticated'::text)));

create policy "Give authenticated users access 1bs1gex_1"
on "storage"."objects"
as permissive
for insert
to public
with check (((bucket_id = 'avatar'::text) AND (auth.role() = 'authenticated'::text)));
