create table "public"."profile" (
    "id" uuid not null,
    "updated_at" timestamp with time zone default now(),
    "passport_name" text,
    "chosen_name" text,
    "gender" text,
    "sweatshirt_size" text,
    "tshirt_size" text
);

alter table "public"."profile" enable row level security;

CREATE UNIQUE INDEX profile_pkey ON public.profile USING btree (id);

alter table "public"."profile" add constraint "profile_pkey" PRIMARY KEY using index "profile_pkey";

alter table "public"."profile" add constraint "profile_id_fkey" FOREIGN KEY (id) REFERENCES auth.users(id) not valid;

alter table "public"."profile" validate constraint "profile_id_fkey";

alter table "public"."profile_type"
add column committee text;

create table "public"."seq_id" (
    "id" uuid not null,
    "seq_id" serial unique
);

alter table "public"."invite_key"
add column committee text;

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.handle_deleted_user()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
 SET search_path TO 'public'
AS $function$
begin
  delete from public.profile_type where id = old.id;
  delete from public.profile where id = old.id;
  return old;
end;
$function$
;

CREATE OR REPLACE FUNCTION public.handle_new_user()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
 SET search_path TO 'public'
AS $function$
begin
  insert into public.profile_type (id, role, country_alpha3, committee)
  values (new.id, new.raw_user_meta_data->>'role', new.raw_user_meta_data->>'country_alpha3', new.raw_user_meta_data->>'committee');
  insert into public.seq_id (id)
  values (new.id);
  insert into public.profile (id)
  values (new.id);
  return new;
end;
$function$
;

create view export_profile as
  select seq_id.seq_id, profile.*, profile_type.role, profile_type.committee, country.name as country_name, country.alpha3 as country_alpha3, auth.users.email, auth.users.email_confirmed_at
  from profile
  join seq_id on profile.id=seq_id.id
  join profile_type on profile.id=profile_type.id
  join auth.users on profile.id=auth.users.id
  join country on profile_type.country_alpha3=country.alpha3;
