create table "public"."country" (
    "alpha3" text not null,
    "name" text not null
);


alter table "public"."country" enable row level security;

create table "public"."invite_key" (
    "key" text not null,
    "role" text not null,
    "country_alpha3" text
);


alter table "public"."invite_key" enable row level security;

create table "public"."role" (
    "role" text not null
);


alter table "public"."role" enable row level security;

CREATE UNIQUE INDEX country_name_key ON public.country USING btree (name);

CREATE UNIQUE INDEX country_pkey ON public.country USING btree (alpha3);

CREATE UNIQUE INDEX invite_key_pkey ON public.invite_key USING btree (key);

CREATE UNIQUE INDEX role_pkey ON public.role USING btree (role);

alter table "public"."country" add constraint "country_pkey" PRIMARY KEY using index "country_pkey";

alter table "public"."invite_key" add constraint "invite_key_pkey" PRIMARY KEY using index "invite_key_pkey";

alter table "public"."role" add constraint "role_pkey" PRIMARY KEY using index "role_pkey";

alter table "public"."country" add constraint "country_name_key" UNIQUE using index "country_name_key";

alter table "public"."invite_key" add constraint "invite_key_country_alpha3_fkey" FOREIGN KEY (country_alpha3) REFERENCES country(alpha3) not valid;

alter table "public"."invite_key" validate constraint "invite_key_country_alpha3_fkey";

alter table "public"."invite_key" add constraint "invite_key_role_fkey" FOREIGN KEY (role) REFERENCES role(role) not valid;

alter table "public"."invite_key" validate constraint "invite_key_role_fkey";


