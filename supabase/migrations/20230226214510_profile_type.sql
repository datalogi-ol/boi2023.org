create table "public"."profile_type" (
    "id" uuid not null,
    "role" text not null,
    "country_alpha3" text
);


alter table "public"."profile_type" enable row level security;

CREATE UNIQUE INDEX profile_type_pkey ON public.profile_type USING btree (id);

alter table "public"."profile_type" add constraint "profile_type_pkey" PRIMARY KEY using index "profile_type_pkey";

alter table "public"."profile_type" add constraint "profile_type_country_alpha3_fkey" FOREIGN KEY (country_alpha3) REFERENCES country(alpha3) not valid;

alter table "public"."profile_type" validate constraint "profile_type_country_alpha3_fkey";

alter table "public"."profile_type" add constraint "profile_type_id_fkey" FOREIGN KEY (id) REFERENCES auth.users(id) not valid;

alter table "public"."profile_type" validate constraint "profile_type_id_fkey";

alter table "public"."profile_type" add constraint "profile_type_role_fkey" FOREIGN KEY (role) REFERENCES role(role) not valid;

alter table "public"."profile_type" validate constraint "profile_type_role_fkey";

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.handle_deleted_user()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
 SET search_path TO 'public'
AS $function$
begin
  delete from public.profile_type where id = old.id;
  return old;
end;
$function$
;

CREATE OR REPLACE FUNCTION public.handle_new_user()
 RETURNS trigger
 LANGUAGE plpgsql
 SECURITY DEFINER
 SET search_path TO 'public'
AS $function$
begin
  insert into public.profile_type (id, role, country_alpha3)
  values (new.id, new.raw_user_meta_data->>'role', new.raw_user_meta_data->>'country_alpha3');
  return new;
end;
$function$
;

CREATE TRIGGER on_auth_user_created AFTER INSERT ON auth.users FOR EACH ROW EXECUTE FUNCTION handle_new_user();

CREATE TRIGGER on_auth_user_deleted BEFORE DELETE ON auth.users FOR EACH ROW EXECUTE FUNCTION handle_deleted_user();
