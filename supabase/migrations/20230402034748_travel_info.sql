alter table "public"."profile"
add column arrival_date date,
add column arrival_time text,
add column arrival_place text,
add column arrival_id text,
add column departure_date date,
add column departure_time text,
add column departure_place text,
add column departure_id text,
add column travel_remarks text;

drop function public.get_export_profile(text);
drop view "public"."export_profile";
create view "public"."export_profile" with (security_invoker) as SELECT seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.birthdate,
    profile.mobile_phone,
    profile.passport_nationality,
    profile.eduroam_access,
    profile.diet_requirements,
    profile.special_needs,
    profile.consent,
    profile.avatar_url,
    profile.arrival_date,
    profile.arrival_time,
    profile.arrival_place,
    profile.arrival_id,
    profile.departure_date,
    profile.departure_time,
    profile.departure_place,
    profile.departure_id,
    profile.travel_remarks,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
   FROM ((((profile
     JOIN seq_id ON ((profile.id = seq_id.id)))
     JOIN profile_type ON ((profile.id = profile_type.id)))
     JOIN auth.users ON ((profile.id = users.id)))
     LEFT JOIN country ON ((profile_type.country_alpha3 = country.alpha3)));

set check_function_bodies = off;

CREATE OR REPLACE FUNCTION public.get_export_profile(sk text)
 RETURNS SETOF export_profile
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
  select seq_id.seq_id,
    profile.id,
    profile.passport_name,
    profile.chosen_name,
    profile.gender,
    profile.sweatshirt_size,
    profile.tshirt_size,
    profile.birthdate,
    profile.mobile_phone,
    profile.passport_nationality,
    profile.eduroam_access,
    profile.diet_requirements,
    profile.special_needs,
    profile.consent,
    profile.avatar_url,
    profile.arrival_date,
    profile.arrival_time,
    profile.arrival_place,
    profile.arrival_id,
    profile.departure_date,
    profile.departure_time,
    profile.departure_place,
    profile.departure_id,
    profile.travel_remarks,
    profile.updated_at,
    profile_type.role,
    profile_type.committee,
    country.name AS country_name,
    country.alpha3 AS country_alpha3,
    users.email,
    users.email_confirmed_at
    from profile
    join seq_id on profile.id=seq_id.id
    join profile_type on profile.id=profile_type.id
    join auth.users on profile.id=auth.users.id
    left join country on profile_type.country_alpha3=country.alpha3
    where exists (select * from server_key where server_key.server_key=sk);
$function$
;
