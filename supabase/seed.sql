INSERT INTO role (role)
VALUES ('contestant'), ('contestant_extra'), ('leader'), ('organizer'), ('guide'), ('guide_applicant'), ('guest');

INSERT INTO country (alpha3, name)
VALUES ('DEU', 'Germany'), ('DNK', 'Denmark'), ('EST', 'Estonia'), ('FIN', 'Finland'), ('ISL', 'Iceland'), ('LVA', 'Latvia'), ('LTU', 'Lithuania'), ('NOR', 'Norway'), ('POL', 'Poland'), ('SWE', 'Sweden'), ('UKR', 'Ukraine');

INSERT INTO invite_key (key, role, country_alpha3, committee)
VALUES ('APPLY', 'guide_applicant', NULL, NULL),
       ('contestant_germany_123', 'contestant', 'DEU', NULL),
       ('contestant_denmark_123', 'contestant', 'DNK', NULL),
       ('contestant_estonia_123', 'contestant', 'EST', NULL),
       ('contestant_extra_denmark_123', 'contestant_extra', 'DNK', NULL),
       ('leader_germany_123', 'leader', 'DEU', NULL),
       ('leader_denmark_123', 'leader', 'DNK', NULL),
       ('leader_estonia_123', 'leader', 'EST', NULL),
       ('organizer_germany_123', 'organizer', 'DEU', NULL),
       ('organizer_denmark_123', 'organizer', 'DNK', NULL),
       ('organizer_hc_denmark_123', 'organizer', 'DNK', 'HC'),
       ('organizer_sc_denmark_123', 'organizer', 'DNK', 'SC'),
       ('organizer_tc_denmark_123', 'organizer', 'DNK', 'TC');

INSERT INTO server_key (server_key)
VALUES ('DySkcEkecwyPRLHHm3pamBWZXM2TW6fd989j2RM3Ughxen4dUSVZBSdCg9BDEJLn8tYW77QvFNTqA3n4NMShUFgv72ewvnD3xGUWyg7KC9yTLeBMrC2TDF7ZTFDRnkkV');
