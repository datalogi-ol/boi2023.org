import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const prerender = true;

export const load = async (event) => {
  const { session } = await getSupabase(event);
  return { session };
};
