import { formatDate } from '$lib/utils';

export async function load({ params }) {
  const post = await import(`../../../../lib/news/${params.slug}.md`);
  const { title, date } = post.metadata;
  const content = post.default;
  const dateString = formatDate(date);

  return {
    content,
    title,
    dateString,
  };
}
