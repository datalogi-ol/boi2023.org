import { supabaseAdmin, get_export_profile } from '$lib/server/db';
import { formatBadgeId } from '$lib/profileFields';
import { redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (!session) throw redirect(303, '/sign-in');
  const id = session.user.id;

  const { data: profileTypeData, error: profileTypeError } = await supabaseAdmin
    .from('profile_type')
    .select('role, committee, country(name, alpha3)')
    .eq('id', id)
    .maybeSingle();
  if (profileTypeError) throw profileTypeError;

  const role = profileTypeData.role;
  const committee = profileTypeData.committee;
  const country_alpha3 = profileTypeData.country?.alpha3;

  let everyonesData = await get_export_profile({ id, role, country_alpha3, committee });
  everyonesData = everyonesData
    .map((personData) => ({
      ...personData,
      badge_id: formatBadgeId(
        personData.country_alpha3,
        personData.role,
        personData.committee,
        personData.badge_id_number,
      ),
    }))
    .sort((a, b) => ('' + a.badge_id).localeCompare(b.badge_id, 'en', { numeric: true }));

  return {
    title: 'Merch tags',
    everyonesData,
  };
};
