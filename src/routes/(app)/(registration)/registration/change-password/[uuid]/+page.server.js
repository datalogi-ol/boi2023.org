import { supabaseAdmin, get_export_profile } from '$lib/server/db';
import { error, fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (!session) throw redirect(303, '/sign-in');
  const myId = session.user.id;

  const { data: myTypeData, error: myTypeError } = await supabaseAdmin
    .from('profile_type')
    .select('role, committee, country(alpha3)')
    .eq('id', myId)
    .maybeSingle();
  if (myTypeError) throw myTypeError;

  const myRole = myTypeData.role;
  const myCommittee = myTypeData.committee;
  const myCountry_alpha3 = myTypeData.country?.alpha3;

  const id = event.params.uuid;

  const profileData = await get_export_profile({
    id: myId,
    role: myRole,
    country_alpha3: myCountry_alpha3,
    committee: myCommittee,
    edit: true,
    theirId: id,
  });
  if (!profileData) throw error(404, 'Not found');

  const email = profileData.email;

  return {
    email,
  };
};

export const actions = {
  default: async (event) => {
    const { request } = event;
    const { session } = await getSupabase(event);
    if (!session) throw error(401, { message: 'Unauthorized' });
    const myId = session.user.id;

    const { data: myTypeData, error: myTypeError } = await supabaseAdmin
      .from('profile_type')
      .select('role, committee, country(alpha3)')
      .eq('id', myId)
      .maybeSingle();
    if (myTypeError) throw myTypeError;

    const myRole = myTypeData.role;
    const myCommittee = myTypeData.committee;
    const myCountry_alpha3 = myTypeData.country?.alpha3;

    const id = event.params.uuid;

    const profileData = await get_export_profile({
      id: myId,
      role: myRole,
      country_alpha3: myCountry_alpha3,
      committee: myCommittee,
      edit: true,
      theirId: id,
    });
    if (!profileData) throw error(404, 'Not found');

    const formData = await request.formData();
    const password = formData.get('password');
    if (!password) return fail(422, { error: 'Please write a password.' });

    const { error } = await supabaseAdmin.auth.admin.updateUserById(id, {
      password,
      email_confirm: true,
    });
    if (error) return fail(error.status, { error: error.message });

    return { message: 'Successfully changed password.' };
  },
};
