import { supabaseAdmin, get_export_profile } from '$lib/server/db';
import { fieldsForRole, roles, countries_alpha3, formatBadgeId } from '$lib/profileFields';
import { error, fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

const numMissingFields = (fields, profileData) => {
  const requiredFields = fields.filter((field) => !field.optional);
  const requiredFilled = requiredFields.filter((field) => profileData[field.id]);
  const num_required = requiredFields.length;
  const num_not_missing = requiredFilled.length;
  const num_missing = num_required - num_not_missing;
  return { num_missing, num_not_missing, num_required };
};

const overviewOfAccounts = (peopleData) => {
  const num = peopleData.length;
  const numNotStarted = peopleData.filter((personData) => !personData.num_not_missing).length;
  const numNotDone = peopleData.filter((personData) => personData.num_missing).length;
  const numHTML =
    num > 0 ? `<td><strong>${num}</strong></td>` : `<td style="color: lightgrey">${num}</td>`;
  const numNotStartedHTML =
    numNotStarted > 0
      ? `<td bgcolor="yellow">${numNotStarted}</td>`
      : `<td style="color: lightgrey">${numNotStarted}</td>`;
  const numNotDoneHTML =
    numNotDone > 0
      ? `<td bgcolor="yellow">${numNotDone}</td>`
      : `<td style="color: lightgrey">${numNotDone}</td>`;
  return { numHTML, numNotStartedHTML, numNotDoneHTML };
};

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (!session) throw redirect(303, '/sign-in');
  const id = session.user.id;
  const email = session.user.email;

  const { data: profileTypeData, error: profileTypeError } = await supabaseAdmin
    .from('profile_type')
    .select('role, committee, country(name, alpha3)')
    .eq('id', id)
    .maybeSingle();
  if (profileTypeError) throw profileTypeError;

  const role = profileTypeData.role;
  const committee = profileTypeData.committee;
  const country_name = profileTypeData.country?.name;
  const country_alpha3 = profileTypeData.country?.alpha3;

  let inviteKeys = [];
  if (committee === 'HC') {
    const { data: inviteKeysData, error: inviteKeysError } = await supabaseAdmin
      .from('invite_key')
      .select('key')
      .in('role', ['leader', 'organizer'])
      .order('key', { ascending: true });
    if (inviteKeysError) throw inviteKeysError;
    inviteKeys = inviteKeysData.map((row) => row.key);
  }
  let everyonesData = await get_export_profile({ id, role, country_alpha3, committee });
  everyonesData = everyonesData
    .map((personData) => ({
      ...personData,
      ...numMissingFields(fieldsForRole(personData.role), personData),
      first_name: personData.chosen_name?.match(/[^\s]*/),
      badge_id: formatBadgeId(
        personData.country_alpha3,
        personData.role,
        personData.committee,
        personData.badge_id_number,
      ),
    }))
    .sort((a, b) => ('' + a.badge_id).localeCompare(b.badge_id, 'en', { numeric: true }));

  const countryOverview = [
    {
      numHTML: overviewOfAccounts(everyonesData),
      numHTMLForRole: roles.map((role) =>
        overviewOfAccounts(everyonesData.filter((personData) => personData.role === role)),
      ),
    },
    ...countries_alpha3.map((country_alpha3) => {
      const countryPeople = everyonesData.filter(
        (personData) => personData.country_alpha3 === country_alpha3,
      );
      const numHTML = overviewOfAccounts(countryPeople);
      const numHTMLForRole = roles.map((role) =>
        overviewOfAccounts(countryPeople.filter((personData) => personData.role === role)),
      );
      return { numHTML, numHTMLForRole };
    }),
  ];

  return {
    title: 'Registration',
    toc: true,
    email,
    role,
    committee,
    country_name,
    inviteKeys,
    everyonesData,
    countryOverview,
  };
};

export const actions = {
  'change-email': async (event) => {
    const { request } = event;
    const { session, supabaseClient } = await getSupabase(event);
    if (!session) throw error(401, { message: 'Unauthorized' });
    const formData = await request.formData();

    const email = formData.get('email');

    const { error } = await supabaseClient.auth.updateUser({ email });
    if (error) return fail(error.status, { email, top_error: error.message });

    return {
      top_message:
        'Please follow the verification links sent to your old email AND your new email!',
    };
  },

  'change-password': async (event) => {
    const { request } = event;
    const { session, supabaseClient } = await getSupabase(event);
    if (!session) throw error(401, { message: 'Unauthorized' });
    const formData = await request.formData();

    const password = formData.get('password');

    const { error } = await supabaseClient.auth.updateUser({ password });
    if (error) return fail(error.status, { top_error: error.message });

    return {
      top_message: 'Password changed.',
    };
  },

  invite: async (event) => {
    const { request } = event;
    const { session } = await getSupabase(event);
    if (!session) throw error(401, { message: 'Unauthorized' });

    const id = session.user.id;

    const { data: profileTypeData, error: profileTypeError } = await supabaseAdmin
      .from('profile_type')
      .select('role, committee, country(name, alpha3)')
      .eq('id', id)
      .maybeSingle();
    if (profileTypeError) throw profileTypeError;

    const myRole = profileTypeData.role;
    const myCommittee = profileTypeData.committee;
    if (myRole != 'leader' && myCommittee != 'HC')
      return fail(422, {
        invite_error: 'You cannot invite people, if you are not a leader or HC member.',
      });

    const myCountry_alpha3 = profileTypeData.country?.alpha3;

    const formData = await request.formData();
    const email = formData.get('invite_email');
    if (!email)
      return fail(422, {
        invite_error: 'Please provide an email',
      });
    const role = formData.get('invite_role');
    if (myRole === 'leader') {
      if (role != 'contestant' && role != 'leader')
        return fail(422, {
          invite_error:
            'As a leader, you can only invite contestants and leaders from your own country. Contact the Host Committee, if this is a problem.',
        });
    }
    const committee = formData.get('invite_committee');
    if (committee && committee.length > 0 && myCommittee != 'HC')
      return fail(422, {
        invite_error:
          'You cannot invite people to a committee, if you are not a HC member yourself.',
      });
    const country_alpha3 = formData.get('invite_country_alpha3') || myCountry_alpha3;
    if (!country_alpha3)
      return fail(422, {
        invite_error: 'You cannot invite people, if you are not assigned to a country.',
      });
    if (myCommittee != 'HC' && country_alpha3 != myCountry_alpha3)
      return fail(422, {
        invite_error:
          'You cannot invite people to a different country, if you are not a HC member.',
      });

    let profilesData = await get_export_profile({ email });
    if (profilesData.length > 0)
      return fail(422, {
        invite_error:
          'Email has already been invited. If they were invited by you, and you want to change their role, then you should delete them first.',
      });

    const { error: signUpError } = await supabaseAdmin.auth.signUp({
      email,
      password: '' + Math.floor(Math.random() * 1000000000000),
      options: {
        data: {
          role,
          country_alpha3,
          committee,
        },
      },
    });
    if (signUpError) return fail(422, { invite_error: signUpError.message });

    return {
      invite_message: `Successfully created account for ${email}. They have been sent a sign-in link, and can otherwise use the "forgot password"-feature. Please inform them, that you have created an account in their email.`,
    };
  },
};
