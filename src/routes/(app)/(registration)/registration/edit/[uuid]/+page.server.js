import { supabaseAdmin, get_export_profile } from '$lib/server/db';
import { fieldsForRole, checkField, fieldArrayToObject } from '$lib/profileFields';
import { error, fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

const numMissingFields = (fields, profileData) => {
  const requiredFields = fields.filter((field) => !field.optional);
  return requiredFields.length - requiredFields.filter((field) => profileData[field.id]).length;
};

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (!session) throw redirect(303, '/sign-in');
  const myId = session.user.id;

  const { data: myTypeData, error: myTypeError } = await supabaseAdmin
    .from('profile_type')
    .select('role, committee, country(alpha3)')
    .eq('id', myId)
    .maybeSingle();
  if (myTypeError) throw myTypeError;

  const myRole = myTypeData.role;
  const myCommittee = myTypeData.committee;
  const myCountry_alpha3 = myTypeData.country?.alpha3;

  const id = event.params.uuid;

  const profileData = await get_export_profile({
    id: myId,
    role: myRole,
    country_alpha3: myCountry_alpha3,
    committee: myCommittee,
    edit: true,
    theirId: id,
  });
  if (!profileData) throw error(404, 'Not found');

  const email = profileData.email;
  const role = profileData.role;
  const committee = myCommittee;
  const country_name = profileData.country_name;

  const ownFields = fieldsForRole(role);
  const numMissingFieldsOwn = numMissingFields(ownFields, profileData);

  return {
    title: `Registration for ${email}`,
    email,
    role,
    committee,
    country_name,
    dbValues: profileData,
    numMissingFields: numMissingFieldsOwn,
  };
};

export const actions = {
  default: async (event) => {
    const { request } = event;
    const { session } = await getSupabase(event);
    if (!session) throw error(401, { message: 'Unauthorized' });
    const myId = session.user.id;

    const { data: myTypeData, error: myTypeError } = await supabaseAdmin
      .from('profile_type')
      .select('role, committee, country(alpha3)')
      .eq('id', myId)
      .maybeSingle();
    if (myTypeError) throw myTypeError;

    const myRole = myTypeData.role;
    const myCommittee = myTypeData.committee;
    const myCountry_alpha3 = myTypeData.country?.alpha3;

    const id = event.params.uuid;

    const profileData = await get_export_profile({
      id: myId,
      role: myRole,
      country_alpha3: myCountry_alpha3,
      committee: myCommittee,
      edit: true,
      theirId: id,
    });
    if (!profileData) throw error(404, 'Not found');

    const role = profileData.role;

    const ownFields = fieldsForRole(role);
    const formData = await request.formData();

    const checkedFields = ownFields
      .map((field) => ({ ...field, value: formData.get(field.id) }))
      .map((field) => ({ ...field, error: checkField(field.id, field.value) }))
      .map((field) => (field.type === 'date' && !field.value ? { ...field, value: null } : field))
      .map((field) =>
        field.id === 'avatar_url' && !field.value ? { ...field, value: null } : field,
      );
    const checkedFieldsObj = fieldArrayToObject(checkedFields);

    if (checkedFields.some((field) => field.error))
      return fail(422, {
        error:
          'Your changes were not saved. Please fix the indicated validation errors and try again.',
        ...checkedFieldsObj,
      });

    const validFields = checkedFields.filter(
      (field) => !field.error && (!field.locked || myCommittee === 'HC'),
    );
    const validFieldsObj = validFields.reduce(
      (acc, field) => ({ ...acc, [field.id]: field.value }),
      {},
    );

    const { error: newProfileError } = await supabaseAdmin
      .from('profile')
      .update({ ...validFieldsObj, updated_at: new Date().toISOString() })
      .eq('id', id);
    if (newProfileError) throw newProfileError;

    return { message: 'Successfully saved.' };
  },
};
