import { supabaseAdmin, get_export_profile } from '$lib/server/db';
import { redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { profileAdminDisabled } from '$lib/profileFields';

export const GET = async (event) => {
  const { session } = await getSupabase(event);
  if (!session || profileAdminDisabled) throw redirect(303, '/sign-in');
  const id = session.user.id;

  const deleteId = event.params.uuid;

  let profilesData = await get_export_profile({ idIn: [id, deleteId] });

  if (profilesData.length != 2)
    throw redirect(303, '/registration?error_description=Invalid+delete+URL');

  const me = profilesData[profilesData[0].id === id ? 0 : 1];
  const other = profilesData[profilesData[0].id === deleteId ? 0 : 1];

  if (me.role != 'leader' && me.committee != 'HC')
    throw redirect(
      303,
      '/registration?error_description=You+cannot+delete+users+unless+you+are+a+leader+or+member+of+HC.',
    );

  if (me.country_alpha3 != other.country_alpha3 && me.committee != 'HC')
    throw redirect(
      303,
      '/registration?error_description=You+can+only+delete+users+from+your+country.',
    );

  await supabaseAdmin.auth.admin.deleteUser(deleteId);
  throw redirect(303, `/registration?message=Successfully+deleted+${other.email}.`);
};
