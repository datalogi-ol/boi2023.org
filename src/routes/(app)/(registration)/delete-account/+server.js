import { supabaseAdmin } from '$lib/server/db';
import { redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';
import { profileAdminDisabled } from '$lib/profileFields';

export const GET = async (event) => {
  const { session } = await getSupabase(event);
  if (!session || profileAdminDisabled) throw redirect(303, '/sign-in');
  const id = session.user.id;
  await supabaseAdmin.auth.admin.deleteUser(id);
  throw redirect(303, `/sign-out`);
};
