import { fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (session) throw redirect(303, '/registration');
};

export const actions = {
  default: async (event) => {
    const { request } = event;
    const { supabaseClient } = await getSupabase(event);
    const formData = await request.formData();

    const email = formData.get('email');

    const { error } = await supabaseClient.auth.resetPasswordForEmail(email);
    if (error) return fail(error.status, { email, error: error.message });

    return {
      message: `Magic link sent to ${email}. Go to your email inbox. Click the link to automatically sign in to your account. From there, you can set a new password.`,
    };
  },
};
