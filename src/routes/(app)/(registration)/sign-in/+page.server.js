import { fail, redirect } from '@sveltejs/kit';
import { getSupabase } from '@supabase/auth-helpers-sveltekit';

export const load = async (event) => {
  const { session } = await getSupabase(event);
  if (session) throw redirect(303, '/registration');
};

export const actions = {
  default: async (event) => {
    const { request } = event;
    const { supabaseClient } = await getSupabase(event);

    const formData = await request.formData();
    const email = formData.get('email');
    const password = formData.get('password');

    const { error } = await supabaseClient.auth.signInWithPassword({ email, password });
    if (error) return fail(error.status, { email, error: error.message });

    throw redirect(303, '/registration');
  },
};
