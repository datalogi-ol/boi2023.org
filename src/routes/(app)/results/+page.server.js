import pkg from 'json-2-csv';
const { csv2jsonAsync } = pkg;

export const load = async ({ fetch }) => {
  let people = [];

  const peopleCsvReadableStream = await fetch('/assets/data/results/final.csv');
  const peopleCsv = await peopleCsvReadableStream.text();
  const peopleJson = await csv2jsonAsync(peopleCsv);

  people = peopleJson.sort(
    (a, b) =>
      -('' + a['TOT']).localeCompare(b['TOT'], 'en', { numeric: true }) ||
      ('' + a['Chosen name']).localeCompare(b['Chosen name']),
  );

  return {
    title: 'Results',
    toc: false,
    people,
  };
};
