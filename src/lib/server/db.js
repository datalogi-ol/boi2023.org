import { createClient } from '@supabase/auth-helpers-sveltekit';
import { env } from '$env/dynamic/public';
import { env as envPrivate } from '$env/dynamic/private';

export const supabaseAdmin = createClient(
  env.PUBLIC_SUPABASE_URL,
  envPrivate.SUPABASE_PRIVATE_KEY,
  {
    auth: {
      autoRefreshToken: false,
      persistSession: false,
    },
  },
);

const sk = envPrivate.SERVER_KEY;

export const get_export_profile = async ({
  id,
  role,
  country_alpha3,
  committee,
  edit,
  theirId,
  email,
  idIn,
}) => {
  let promise = supabaseAdmin.rpc('get_export_profile', { sk });

  if (email) promise = promise.eq('email', email);
  else if (idIn) promise = promise.in('id', idIn);
  else if (committee === 'HC');
  else if (role === 'leader')
    promise = promise
      .eq('country_alpha3', country_alpha3)
      .in('role', ['contestant', 'contestant_extra', 'leader']);
  else promise = promise.eq('id', id);

  if (edit) promise = promise.eq('id', theirId).maybeSingle();

  const { data, error } = await promise;

  if (error) throw error;
  return data;
};
