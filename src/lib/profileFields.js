const profileFields = [
  {
    id: 'passport_name',
    type: 'text',
    display: 'Name as in passport',
    maxLength: 200,
    roles: 'clog',
    locked: true,
  },
  {
    id: 'chosen_name',
    type: 'text',
    display: 'Chosen name',
    description:
      'Your first name and surname, as you would like it to be printed on your ID badge and on scoreboards.',
    maxLength: 30,
    roles: 'clog',
    locked: true,
  },
  {
    id: 'avatar_url',
    type: 'avatar',
    display: 'Profile picture',
    description:
      'Please upload a .jpg-file with aspect ratio 3:4. It must contain only your face, and your face must be clearly visible. This will be made publicly visible.',
    roles: 'clog',
    locked: true,
  },
  {
    id: 'gender',
    type: 'select',
    options: ['Female', 'Male', 'Other'],
    display: 'Gender',
    description:
      'Will be used to partition participants into hotel rooms. It will not be otherwise published.',
    roles: 'clog',
    locked: 'true',
  },
  {
    id: 'sweatshirt_size',
    type: 'select',
    options: ['XS', 'S', 'M', 'L', 'XL', '2XL', '3XL'],
    display: 'Sweatshirt size',
    description: 'All sweatshirts are unisex.',
    images: ['/assets/img/merch/sweatshirt_sizes.png'],
    roles: 'clog',
    locked: 'true',
  },
  {
    id: 'tshirt_size',
    type: 'select',
    options: [
      'Female XS',
      'Female S',
      'Female M',
      'Female L',
      'Female XL',
      'Female 2XL',
      'Male S',
      'Male M',
      'Male L',
      'Male XL',
      'Male 2XL',
      'Male 3XL',
    ],
    display: 'T-shirt size',
    description: 'You can choose either a female or male model.',
    images: [
      '/assets/img/merch/tshirt_female_sizes.png',
      '/assets/img/merch/tshirt_male_sizes.png',
    ],
    roles: 'clog',
    locked: 'true',
  },
  {
    id: 'birthdate',
    type: 'date',
    display: 'Date of birth',
    roles: 'clog',
    locked: true,
  },
  {
    id: 'mobile_phone',
    type: 'text',
    display: 'Mobile phone',
    maxLength: 30,
    roles: 'clog',
  },
  {
    id: 'passport_nationality',
    type: 'text',
    display: 'Nationality according to passport',
    maxLength: 200,
    roles: 'clog',
    locked: true,
  },
  {
    id: 'eduroam_access',
    type: 'select',
    options: ['Yes', 'No'],
    display: 'Eduroam Access',
    roles: 'l',
  },
  {
    id: 'diet_requirements',
    type: 'textarea',
    display: 'Diet requirements',
    maxLength: 300,
    optional: true,
    roles: 'clog',
    locked: 'true',
  },
  {
    id: 'special_needs',
    type: 'textarea',
    display: 'Special needs / other requirements',
    maxLength: 500,
    optional: true,
    roles: 'clog',
    locked: 'true',
  },
  {
    id: 'consent',
    type: 'select',
    options: ['Yes', 'No'],
    display: 'Consent',
    description:
      'Do you consent to the publication of your chosen name, photo, as well as your point and medal results on our website?',
    roles: 'clog',
    locked: true,
  },
  {
    id: 'arrival_date',
    type: 'date',
    display: 'Travel info: Date of arrival',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    optional: true,
    roles: 'cl',
  },
  {
    id: 'arrival_time',
    type: 'text',
    display: 'Travel info: Time of arrival',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 50,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'arrival_place',
    type: 'textarea',
    display: 'Travel info: Where will you be arriving?',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 100,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'arrival_id',
    type: 'textarea',
    display: 'Travel info: ID of arriving train/ship/flight',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 100,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'departure_date',
    type: 'date',
    display: 'Travel info: Date of departure',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    optional: true,
    roles: 'cl',
  },
  {
    id: 'departure_time',
    type: 'text',
    display: 'Travel info: Time of departure',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 50,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'departure_place',
    type: 'textarea',
    display: 'Travel info: From where will you be departing?',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 100,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'departure_id',
    type: 'textarea',
    display: 'Travel info: ID of departing train/ship/flight',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 100,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'travel_remarks',
    type: 'textarea',
    display: 'Travel info: Remarks',
    description:
      'If everyone on the team travels together, it is sufficient to fill out travel info for one leader.',
    maxLength: 200,
    optional: true,
    roles: 'cl',
  },
  {
    id: 'guide_application',
    type: 'textarea',
    display: 'Questions for coming guides',
    description: `Vi er glade for, at du er her!
       Det er formegentlig muligt at blive guide, hvis du vil det.
       Vi har lige nogle spørgsmål først, så vi kan forventningsafstemme lidt.
       <br/>
       BOI er en årlig programmeringskonkurrence for gymnasieelever fra 11 lande.
       I år er det Danmarks tur til, være vært.
       Derfor søger vi frivillige studerende, der kan være såkaldte "guides" for vores omkring 100 gæster.
       Hvert land, bestående af 6 gymnasieelever og 1-2 ældre holdledere skal have en guide.
       <br/>
       BOI 2023 foregår over 5 dage.
       Første dag er ankomstdag, og der vil være en åbningsceremoni og en kort øve-konkurrence.
       De næste to dage er konkurrencedagene, hvor der hver dag er en 5-timers konkurrence og derefter noget socialt hygge.
       Fjerde dag går med ekskursion til København, en afslutningsceremoni og en festmiddag.
       Femte dag er til afrejse.
       <br/><br/>
       Der er flere typer opgaver, vi har brug for frivillige til.
       Alle roller vil have noget overlap og fleksibilitet.
       Nogle roller er der ikke altid brug for, så nogle vil skifte rolle undervejs.
       <br/><br/>
       Som <strong>Team Guide</strong> er dine opgave, at
       <ul>
         <li>hjælpe dit hold med, at finde rundt på DTU og følge tidsplanen. F.eks. når de skal gå fra konkurrencelokalerne til frokost.</li>
         <li>være der til, at snakke med og holde lidt styr på deltagerne, når holdlederne (ofte) er til møde.</li>
         <li>være en dansker, som dit hold kan snakke og hygge sig med, og opleve noget kulturel udveksling.</li>
         <li>være til rådighed, når organisatorerne har brug for hjælp til mindre praktiske opgaver.</li>
         <li>hjælpe dit hold med, at få et indblik i Danmark, når vi tager på en ekskursion. Der er også to danske hold, men ikke alle kender til København.</li>
         <li>hygge dig med de andre guides, når f.eks. deltagerne sidder til konkurrence. Forhåbentlig kan du lære nogle nye venner at kende :)</li>
       </ul>
       Vi får hold fra Tyskland, Danmark, Estland, Finland, Island, Letland, Litauen, Norge, Polen, Sverige og Ukraine.
       <br/>
       Som <strong>Leader Guide</strong> er dine opgaver, at
       <ul>
         <li>hjælpe ledere med, at finde rundt på DTU og i København.</li>
         <li>tale med de dygtige og sympatiske ledere, hvis du har lyst.</li>
         <li>hjælpe med andre opgaver.</li>
       </ul>
       Som <strong>Event Guide</strong> er dine opgaver, at
       <ul>
         <li>have ansvar for større og mindre sociale aktiviteter og lege.</li>
         <li>hjælpe til på større events, ceremonier og ekskursioner.</li>
       </ul>
       Som <strong>Meal Guide</strong> er dine opgaver, at
       <ul>
         <li>være praktisk hjælper, når vi skal stille mad op og væk. Deltagerne spiser morgenmad på hotellet, men resten skal vi selv sørge for, ved hjælp af cateringfirmaer. Maden bliver bestilt af organisatorerne, men vi har brug for hjælp til, at det bliver en god oplevelse, når maden skal fordeles ud på de mange gæster.</li>
       </ul>
       Som <strong>Hotel Guide</strong> er dine opgaver, at
       <ul>
         <li>overnatte med deltagere og ledere på Zleep Hotel meget tæt på DTU.</li>
         <li>tage sig af problemer der måtte opstå på hotellet.</li>
         <li>hygge sig med deltagerne på hotellet om aftenen.</li>
         <li>få folk af sted efter morgenmaden.</li>
       </ul>
       Som <strong>Transport Guide</strong> er dine opgaver, at
       <ul>
         <li>hjælpe holdene med deres transport til og fra DTU og ekskursioner.</li>
       </ul>

       Besvar venligst følgende spørgsmål i feltet herunder.
       <ol>
         <li>Hvordan hørte du om, at være guide til BOI 2032?</li>
         <li>Er du studerende? Hvad og hvor studerer du?</li>
         <li>Hvor gammel er du?</li>
         <li>Kender du allerede til DTUs campus? (Ikke en nødvendighed!)</li>
         <li>Har du mulighed for, at tage 28. april til 1. maj fri? Har du mulighed for, at sove hjemme ved dig selv?</li>
         <li>Hvorfor vil du gerne være guide?</li>
         <li>Er der nogen roller (Team/Leader/Event/Meal/Hotel/Transport) du foretrækker? Er der nogen lande du ville foretrække at guide? Vi vil forsøge, at fordele efter bedste evne. Som sagt må der forventes blandede opgaver.</li>
       </ol>
       På forhånd tak! Hvad du skriver her vil være synligt for organisatorer i Host Committee.`,
    maxLength: 2000,
    roles: 'a',
  },
];

const roleAbbreviation = {
  contestant: 'c',
  contestant_extra: 'c',
  leader: 'l',
  organizer: 'o',
  guide: 'g',
  guide_applicant: 'a',
};

export const roles = [
  'contestant',
  'contestant_extra',
  'leader',
  'organizer',
  'guide',
  'guide_applicant',
];
export const countries_alpha3 = [
  'DEU',
  'DNK',
  'EST',
  'FIN',
  'ISL',
  'LVA',
  'LTU',
  'NOR',
  'POL',
  'SWE',
  'UKR',
];
export const alpha3ToName = {
  DEU: 'Germany',
  DNK: 'Denmark',
  EST: 'Estonia',
  FIN: 'Finland',
  ISL: 'Iceland',
  LTU: 'Lithuania',
  LVA: 'Latvia',
  NOR: 'Norway',
  POL: 'Poland',
  SWE: 'Sweden',
  UKR: 'Ukraine',
};

export const roleHasExportField = (role, field) =>
  !field.roles || field.roles.includes(roleAbbreviation[role]);

export const roleHasProfileField = (role, field) => field.roles?.includes(roleAbbreviation[role]);

export const fieldsForRole = (role) =>
  profileFields.filter((field) => field.roles.includes(roleAbbreviation[role]) && !field.hide);

export const fieldArrayToObject = (fieldArray) =>
  fieldArray.reduce((acc, field) => ({ ...acc, [field.id]: field }), {});

const profileFieldsObj = fieldArrayToObject(profileFields);

export const checkField = (id, value) => {
  if (value === null || value.length === 0) return false;

  const field = profileFieldsObj[id];

  if (field.maxLength && value.length > field.maxLength)
    return `Please use no more than ${field.maxLength} characters. You used ${value.length}.`;

  if (field.type === 'select' && !field.options.includes(value)) return 'Invalid selected value.';
};

export const exportFields = [
  {
    id: 'num_missing',
    display: 'Missing required fields',
  },
  {
    id: 'badge_id',
    display: 'Badge ID',
  },
  {
    id: 'seq_id',
    display: 'Database ID',
  },
  {
    id: 'role',
    display: 'Role',
  },
  {
    id: 'committee',
    display: 'Committee',
  },
  {
    id: 'country_name',
    display: 'Country',
  },
  {
    id: 'country_alpha3',
    display: 'Country alpha3',
  },
  {
    id: 'email',
    display: 'Email',
  },
  ...profileFields,
  {
    id: 'updated_at',
    display: 'Last updated at',
  },
  {
    id: 'first_name',
    display: 'First name (computed from chosen name)',
  },
];

export const profileAdminDisabled = true;

export const formatBadgeId = (country_alpha3, role, committee, badge_id_number) => {
  if (role === 'contestant' || role === 'contestant_extra')
    return `${country_alpha3}-${badge_id_number}`;
  else if (role === 'leader') return `${country_alpha3}-TL`;
  else if (committee) return `${committee}`;
  else if (role === 'organizer') return `${country_alpha3}-ORG`;
  else if (role === 'guide') return `GUIDE-${badge_id_number}`;
};
