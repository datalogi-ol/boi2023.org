---
title: The website is up
date: 2022-03-23
---

We hope to see you at BOI 2023! In the near future, this site will be expanded with participating teams and probably the rules. When we get closer to April 28th 2023, you will also discover a detailed schedule, as well as a registration page, if you are one of the lucky ones. With time, even a page with tasks and results will pop up.

If you have a chance of qualifying, go practice, 'cause this is going to be a great BOI 😀
