---
title: Rules and environment
date: 2023-04-18
---

The [rules](/rules-environment#rules) and information about the [environment](/rules-environment#environment) is now available on the [rules page](/rules-environment).
