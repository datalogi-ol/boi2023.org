---
title: Tasks available on Open Kattis
date: 2024-01-22
---

The BOI 2023 contest tasks have been released on Open Kattis:<br/>
[https://open.kattis.com/problem-sources/BOI 2023](https://open.kattis.com/problem-sources/BOI%202023)
