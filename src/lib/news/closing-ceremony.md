---
title: Closing ceremony schedule
date: 2023-04-30
---

We look forward to seeing you at the closing ceremony!
Here we share the schedule for the evening.

<center>
  <img src="/assets/img/closing-ceremony/closing-ceremony-schedule.png" style="max-height: 45rem" />
</center>
