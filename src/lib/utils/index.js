export const formatDate = (date) => new Date(date).toLocaleDateString('en-GB');

export const fetchMarkdownPosts = async () => {
  // Heavily inspired by https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog
  const allPostFiles = import.meta.glob('/src/lib/news/*.md');
  const iterablePostFiles = Object.entries(allPostFiles);

  const allPosts = await Promise.all(
    iterablePostFiles.map(async ([path, resolver]) => {
      const { metadata } = await resolver();
      const postPath = path.slice(9, -3);

      return {
        meta: metadata,
        path: postPath,
      };
    }),
  );

  const sortedPosts = allPosts.sort((a, b) => {
    return new Date(b.meta.date) - new Date(a.meta.date);
  });

  return sortedPosts;
};
